export const addTodo = (value) => ({
	type: "UPDATE_LIST",
	payload: value
});
export default addTodo;
