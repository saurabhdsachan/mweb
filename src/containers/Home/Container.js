import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Header from "../../components/Header/Header";
import NoticeBar from "../../components/NoticeBar/NoticeBar";
import Icon from "../../components/Icon/Icon";
import { Primary, Danger, Success } from "../../components/Button/Button";
import { Checkbox, Radio } from "../../components/OptionPickers/OptionPickers";
import { addTodo } from "../../actions/list";
import { itemsFetchData } from "../../actions/networkFetch";

// Load Style Below
import "./style.scss";

class HomeContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: {
				background: "#ffffff"
			},
			loading: false,
			checkboxOptions: ["#FFFFFF", "#FFB399", "#FF33FF", "#FFFF99", "#00B3E6", "#E6B333", "#3366E6"],
			checkboxOptionsSelected: ["#FFB399","#3366E6"],
			checkboxOptionsDisabled: ["#FFFFFF", "#FF33FF", "#00B3E6", "#3366E6"],
			radioOptions: ["1", "2", "3"],
			radioOptionsSelected: "3",
			radioOptionsDisabled: ["3"]
		};
	}
	handleCheckboxChange = value => {
		const { checkboxOptionsSelected } = this.state;
		const valueIndex = checkboxOptionsSelected.indexOf(value);
		if (valueIndex !== -1) {
			checkboxOptionsSelected.splice(valueIndex, 1);
			this.setState({
				checkboxOptionsSelected: [...checkboxOptionsSelected]
			});
		} else {
			this.setState({
				checkboxOptionsSelected: [...checkboxOptionsSelected, value]
			});
		}
	};
	handleRadioChange = value => {
		this.setState(
			{
				radioOptionsSelected: value
			},
			() => {
				this.props.updateList(value);
			}
		);
	};
	render() {
		const { data, checkboxOptions, checkboxOptionsSelected, checkboxOptionsDisabled, radioOptions, radioOptionsSelected, radioOptionsDisabled } = this.state;
		return (
			<div style={{ background: data.background }}>
				<Header />
				<div className="yo-container">
					<div className="yo-row full-width">
						{this.props.items && this.props.items.map(item => <NoticeBar key={`notice-${item.id}`} title={item.name} message={item.body} />)}
					</div>
					<div className="yo-row full-width">
						<Link to="/about">About</Link>
						<Link to="/login">Login</Link>
					</div>
					<div className="yo-row">
						<Primary type="button" buttonType="primary" disabled text={`Total No of Slides : ${this.props.list}`} onClick={() => true}>
						<Icon iconName="sun" />
						</Primary>
						<Danger type={"alert"} text={"Add more slides"}>
							<Icon iconName="bus" />
						</Danger>
						<Success type={"danger"} text={process.env.NODE_ENV} onClick={() => this.props.fetchData("https://jsonplaceholder.typicode.com/comments")}>
							<Icon iconName="checkmark-circle" />
						</Success>
						<Checkbox
							sectionTitle="Chaechbox Example"
							baseId="checkbox-example"
							name="checkboxName"
							options={checkboxOptions}
							selectedOptions={checkboxOptionsSelected}
							disabledOptions={checkboxOptionsDisabled}
							onChange={this.handleCheckboxChange}
							inline
						/>
						<Radio
							sectionTitle="Radio Example"
							baseId="radio-example"
							name="radioName"
							options={radioOptions}
							selectedOptions={radioOptionsSelected}
							disabledOptions={radioOptionsDisabled}
							onChange={this.handleRadioChange}
							inline
						/>
					</div>
				</div>
			</div>
		);
	}
}
HomeContainer.defaultProps = {
	list: "0",
	items: []
};
HomeContainer.propTypes = {
	list: PropTypes.string.isRequired,
	updateList: PropTypes.func.isRequired,
	fetchData: PropTypes.func.isRequired,
	items: PropTypes.arrayOf(
		PropTypes.shape({
			postId: PropTypes.number,
			id: PropTypes.number,
			name: PropTypes.string,
			email: PropTypes.string,
			body: PropTypes.string
		})
	)
};
const mapStateToProps = state => ({
	list: state.ListReducer.list,
	items: state.items
});
const mapDispatchToProps = dispatch => ({
	updateList: value => dispatch(addTodo({ list: value })),
	fetchData: url => dispatch(itemsFetchData(url))
});
export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
