import React from "react";
import {Primary} from "../../components/Button/Button";
import FormInputRow from "../../components/FormInputRow"

// Load Style Below
import "./style.scss";

const LoginContainer = () => (
  <div className="login-wrapper">
    <div className="yo-container">
      <div className="yo-row">
        <div className="form-wrapper">
          <form action="">
            <fieldset>
              <legend>SIGNUP</legend>
              <FormInputRow
              fieldName="userName"
              inputType="text"
              labelText="User Full Name"
              placeHolder="Firstname + Middlename + Lastname"
              helpText="Please enter full name"/>
              <FormInputRow
              fieldName="mobileNumber"
              inputType="nubmer"
              labelText="Mobile Number"
              placeHolder="Mobile Number"
              helpText="Please enter only mobile number"/>
              <FormInputRow
              fieldName="password"
              inputType="password"
              labelText="Password"
              placeHolder="Password"
              helpText="Please enter Password"/>
              <Primary
                text={"Submit"}
              />
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
);


export default LoginContainer;
