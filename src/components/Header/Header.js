import React from "react";
import Icon from "../Icon/Icon";
import logo from "./images/logo.png";
import "./Header.scss";

const Header = () => (
	<div className="app-header">
		<div className="yo-container">
			<div className="yo-row">
				<div className="header-block">
					<div className="brand-logo">
						<img src={logo} alt="logo" />
					</div>
					<div className="brand-or-page-name">
						<h4 className="brand-name">MWEB</h4>
						<small>Second Info</small>
					</div>
					<div className="user-profile-icon">
						<Icon iconName="user"/>
					</div>
				</div>
			</div>
		</div>
	</div>
);

export default Header;
