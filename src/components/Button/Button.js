import React from "react";
import PropTypes from "prop-types";
import "./Button.scss";

const TYPES = {
	PRIMARY: "primary",
	WARNING: "warning",
	DANGER: "danger",
	SUCCESS: "success"
};

const Button = (props) => (
	<button type={props.type} disabled={props.disabled} onClick={props.onClick} className={`btn ${props.buttonType}`}>
		{props.children} {props.text}
	</button>
);
export const Primary = props => <Button {...props} buttonType={TYPES.PRIMARY} />;
export const Warning = props => <Button {...props} buttonType={TYPES.WARNING} />;
export const Danger = props => <Button {...props} buttonType={TYPES.DANGER} />;
export const Success = props => <Button {...props} buttonType={TYPES.SUCCESS} />;

Button.defaultProps = {
	type: "button",
	disabled: false,
	children:null,
	onClick: () => true
};

Button.propTypes = {
	type: PropTypes.string,
	text: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired,
	buttonType: PropTypes.string.isRequired,
	disabled: PropTypes.bool,
	children: PropTypes.element
};
