import React from "react";
import { Route } from "react-router-dom";
import Loadable from "react-loadable";

import "./App.scss";

const Loading = () => <div className="page-in-load">Loading...</div>;
const HomeContainer = Loadable({
  loader: () => import("./Home/Container"),
  loading: Loading
});

const AboutContainer = Loadable({
  loader: () => import("./About/Container"),
  loading: Loading
});
const LoginContainer = Loadable({
  loader: () => import("./Login/Container"),
  loading: Loading
});

const App = () => (
  <div className="">
    <Route path="/" exact component={HomeContainer} />
    <Route path="/about" exact component={AboutContainer} />
    <Route path="/login" exact component={LoginContainer} />
  </div>
);

export default App;
