import { combineReducers } from "redux";
import {
  items,
  itemsHaveError,
  itemsAreLoading
} from "./reducers/networkFetch";
import ListReducer from "./reducers/list";

export default combineReducers({
  items,
  itemsHaveError,
  itemsAreLoading,
  ListReducer
});
