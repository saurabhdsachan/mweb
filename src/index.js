import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
// import initReactFastclick from "react-fastclick";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import App from "./containers/App";
import registerServiceWorker from "./registerServiceWorker";

import "./index.scss";

// initReactFastclick();
const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
