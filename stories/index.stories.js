import React from "react";

import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";

import { Welcome } from "@storybook/react/demo";
import Button from "../src/components/Button/Button";

storiesOf("Welcome", module).add("to Storybook", () => (
	<Welcome showApp={linkTo("Button","with aksjdh text")} />
));

storiesOf("Button", module)
	.add("with text", () => (
		<Button
			btnRoleColor={"danger"}
			value={process.env.NODE_ENV}
			onClick={action("clicked")}
		/>
	)).add("with aksjdh text", () => (
		<Button
			btnRoleColor={"danger"}
			value={"slkhjasd"}
			onClick={action("clicked")}
		/>
	));
