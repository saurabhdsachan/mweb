import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
// Load components
import Header from "../../components/Header/Header";
import NoticeBar from "../../components/NoticeBar/NoticeBar";
import HCard from "../../components/HCard/HCard";
import {Primary} from "../../components/Button/Button";
import { itemsFetchData } from "../../actions/networkFetch";
// Load Style Below
import "./style.scss";

class AboutContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        title: "Alert: ",
        message:
          "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
      },
      loading: false
    };
  }
  componentDidMount() {
    this.props.fetchData("https://jsonplaceholder.typicode.com/posts/1");
  }
  LISTUI = () => {
    const count = this.props.list;
    const arr = [];
    for (let i = 0; i < count; i += 1) {
      arr.push(<HCard key={`ss${i}`} id={i} message="sas" />);
    }
    return arr;
  };

  render() {
    if (this.props.hasError) {
      return <p>Sorry! There was an error loading the items</p>;
    }

    if (this.props.isLoading) {
      return <p>Loading…</p>;
    }
    const { data } = this.state;
    return (
      <div>
        <Header />
        <span>{this.props.info.saurabh}</span>
        <div className="yo-container">
          <div className="yo-row full-width">
            <NoticeBar title={data.title} message={data.message} />
          </div>
          <div className="yo-row full-width">
            <h4 className="top-h-row-name">
              Hello Saurabh, How are you today?
            </h4>
            <div className="top-h-row">{this.LISTUI()}</div>
          </div>
          <div className="yo-row">
            <h1>About {this.props.list}</h1>
            <span>This Data is coming from API: {this.props.items.title}</span>
            <Primary btnRoleColor={"success"} text={"Test Button"} />
            <Link to="/">Home</Link>
          </div>
        </div>
      </div>
    );
  }
}
AboutContainer.defaultProps = {
  list: 0,
  info: {},
  items: []
};
AboutContainer.propTypes = {
  fetchData: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.shape({
        userId: PropTypes.number.isRequired,
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired
      })
    )
  ).isRequired,
  hasError: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  list: PropTypes.number.isRequired,
  info: PropTypes.objectOf(
    PropTypes.shape({
      saurabh: PropTypes.string.isRequired
    })
  )
};
const mapStateToProps = state => ({
  list: state.HomeReducer.data,
  info: state.LoginReducer.info,
  items: state.items,
  hasError: state.itemsHaveError,
  isLoading: state.itemsAreLoading
});
const mapDispatchToProps = dispatch => ({
  fetchData: url => dispatch(itemsFetchData(url))
});
export default connect(mapStateToProps, mapDispatchToProps)(AboutContainer);
