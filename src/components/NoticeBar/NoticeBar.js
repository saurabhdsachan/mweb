import React from "react";
import PropTypes from "prop-types";
import "./NoticeBar.scss";

const NoticeBar = ({ title, message }) => (
	<div className="notice">
		<h4>{title}</h4>
		<small>{message}</small>
	</div>
);

NoticeBar.propTypes = {
	title: PropTypes.string.isRequired,
	message: PropTypes.string.isRequired
};

export default NoticeBar;
