import React, { Fragment } from "react";
import PropTypes from "prop-types";

// Load Style Below
import "./OptionPickers.scss";

const TYPES = {
	CHECKBOX: "checkbox",
	RADIO: "radio"
};

const OptionPickers = props => (
	<Fragment>
		<h3 className="form-label">{props.sectionTitle}</h3>
		{props.options.map((opt, index) => {
			const className = (type, inline) => {
				let finalClass = `custom-${type}`;
				if (inline) {
					finalClass = `${finalClass} inline`;
				}
				return finalClass;
			};
			return (
				<div key={opt} className={className(props.type, props.inline)}>
					{props.type === TYPES.CHECKBOX && (
						<label htmlFor={`${props.baseId}-${index}`} key={opt}>
							{/* To do fix code duplication */}
							<input
								disabled={props.disabledOptions.indexOf(opt) > -1}
								id={`${props.baseId}-${index}`}
								className="form-checkbox"
								name={props.name}
								defaultValue={opt}
								checked={props.selectedOptions.indexOf(opt) > -1}
								type={props.type}
								onChange={() => {
									props.onChange(opt);
								}}
							/>{" "}
							<span className="checkbox-material">
								<span className="check" />
							</span>
							{opt}
						</label>
					)}
					{props.type === TYPES.RADIO && (
						<Fragment>
							<input
								disabled={props.disabledOptions.indexOf(opt) > -1}
								id={`${props.baseId}-${index}`}
								className="form-checkbox"
								name={props.name}
								defaultValue={opt}
								checked={props.selectedOptions.indexOf(opt) > -1}
								type={props.type}
								onChange={() => {
									props.onChange(opt);
								}}
							/>
							<label htmlFor={`${props.baseId}-${index}`} key={opt}>
								{opt}
							</label>
						</Fragment>
					)}
				</div>
			);
		})}
	</Fragment>
);

export const Checkbox = props => <OptionPickers {...props} type={TYPES.CHECKBOX} />;
export const Radio = props => <OptionPickers {...props} type={TYPES.RADIO} />;

OptionPickers.defaultProps = {
	inline: false,
	disabledOptions: []
};

OptionPickers.propTypes = {
	inline: PropTypes.bool,
	baseId: PropTypes.string.isRequired,
	sectionTitle: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	options: PropTypes.arrayOf(PropTypes.string).isRequired,
	disabledOptions: PropTypes.arrayOf(PropTypes.string),
	selectedOptions: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]).isRequired
};
