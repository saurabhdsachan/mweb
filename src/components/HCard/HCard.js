import React from "react";
import PropTypes from "prop-types";
import "./HCard.scss";

const HCard = ({ id, message }) => (
  <div className="card">
    <img alt={message} src={`http://lorempixel.com/200/200/sports/${id}/`} />
  </div>
);
HCard.propTypes = {
  id: PropTypes.number.isRequired,
  message: PropTypes.string.isRequired
};
export default HCard;
