import React from "react";
import PropTypes from "prop-types";

const FormInputRow = ({ fieldName, inputType, labelText, placeHolder, helpText }) => (
	<div className={"form-row"}>
		<label htmlFor={fieldName}>
			{labelText}
			<input className={"form-control"} id={fieldName} type={inputType} placeholder={placeHolder} />
			<span className={"help-text"}>{helpText}</span>
		</label>
	</div>
);

FormInputRow.propTypes = {
	fieldName: PropTypes.string.isRequired,
	inputType: PropTypes.string.isRequired,
	labelText: PropTypes.string.isRequired,
	placeHolder: PropTypes.string.isRequired,
	helpText: PropTypes.string.isRequired
};

export default FormInputRow;
