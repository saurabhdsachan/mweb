import React from "react";
import PropTypes from "prop-types";
import "./Icon.scss";

const Icon = ({ iconName }) => <i className={`lnr lnr-${iconName === "" ? "sad" : iconName}`} />;

Icon.defaultProps = {
	iconName: "sad"
};

Icon.propTypes = {
	iconName: PropTypes.string
};

export default Icon;
