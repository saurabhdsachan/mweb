const ListReducer = (state = {}, action) => {
	switch (action.type) {
		case "UPDATE_LIST":
			return {
				...state,
				list: action.payload.list
			};
		default:
			return state;
	}
};

export default ListReducer;
